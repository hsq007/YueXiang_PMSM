#ifndef __MOTOR_CONTROL_H
#define __MOTOR_CONTROL_H

#include "main.h"

/* PWM周期配置,通过PWM控制电流 */
#define MOTOR_DCT_TIME_BASE 1110

/******************机械刹车的开关控制 // M1_DCT--PC12  M2_DCT--PC9 *******************/
#define MOTORA_DCT_OPEN() gpio_bit_set(GPIOC, GPIO_PIN_12)         /* 打开抱闸 */
#define MOTORA_DCT_CONTONU_OPEN() gpio_bit_set(GPIOC, GPIO_PIN_12) /* 持续打开抱闸 */
#define MOTORA_DCT_CLOSE() gpio_bit_reset(GPIOC, GPIO_PIN_12)      /* 关闭抱闸 */

#define MOTORB_DCT_OPEN() gpio_bit_set(GPIOC, GPIO_PIN_9)         /* 打开抱闸 */
#define MOTORB_DCT_CONTONU_OPEN() gpio_bit_set(GPIOC, GPIO_PIN_9) /* 持续打开抱闸 */
#define MOTORB_DCT_CLOSE() gpio_bit_reset(GPIOC, GPIO_PIN_9)      /* 关闭抱闸 */
/*******************************************************/

#define MOTOR_POWER_24V_OPEN() gpio_bit_set(GPIOD, GPIO_PIN_0)    /* 开电机驱动24V电源 */
#define MOTOR_POWER_24V_CLOSE() gpio_bit_reset(GPIOD, GPIO_PIN_0) /* 关 */

extern u8 motor_speed;

void Init_Motor_DCT(void);

void Motor_Start_Forward(void);
void Motor_Start_Backward(void);
void Motor_Stop_Forward(void);
void Motor_Stop_Backward(void);
void Motor_Stop(void);

void MotorA_DCT_Open(void);
void MotorA_DCT_Close(void);
void MotorB_DCT_Open(void);
void MotorB_DCT_Close(void);

void Motor_Stop_Error(void);

#endif
