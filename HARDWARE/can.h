#ifndef __CAN_H
#define __CAN_H

#include "main.h"

extern FlagStatus receive_flag;
extern uint8_t transmit_number;
extern can_receive_message_struct receive_message;
extern can_trasnmit_message_struct transmit_message;

#define CANx CAN0

void Init_Can(void);

u8 CAN1_Send_Msg(u8 *msg, u8 len);
u8 CAN1_Receive_Msg(u8 *buf);

#endif
